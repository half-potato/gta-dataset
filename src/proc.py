import numpy as np
import cv2
import math
from shapely.geometry import Polygon
import shapely.affinity
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import json, time, sys, math

MIN_POINTS = 600
ANGLE_THRESHOLD = math.radians(45)
DIST_THRESHOLD = 1
MAX_DIST_THRESHOLD = 10
MIN_DIST_THRESHOLD = 0.8
OVERLAP_THRESHOLD = 0.10

paths = [
    ("data/run0/meta%i.json", "data/run0/image%i.png", 18646),
    ("data/run1/meta%i.json", "data/run1/image%i.png", 19694),
    ("data/run2/meta%i.json", "data/run2/image%i.png", 16146),
    ("data/run3/meta%i.json", "data/run3/image%i.png", 27375),
    ]

WIDTH = 2
DIST = 2.5
HEIGHT = 1.5
FOV = 90
TAN = math.tan(math.degrees(FOV/2))
POLYGON_ARR = [
    # Top left, clockwise
    [-WIDTH/2, DIST],
    [WIDTH/2, DIST],
    [-TAN*(DIST-HEIGHT), DIST-HEIGHT],
    [TAN*(DIST-HEIGHT), DIST-HEIGHT],
]
POLYGON = Polygon(POLYGON_ARR)

def calc_overlap(p1, p2):
    return p1.intersection(p2).area / p1.union(p2).area

def create_polygon(x, y, heading):
    rot = shapely.affinity.rotate(POLYGON, heading)
    return shapely.affinity.translate(rot, x, y)

def load_metadata(path, number):
    bar_width = 40
    sys.stdout.write("[%s]" % (" " * bar_width))
    sys.stdout.flush()
    sys.stdout.write("\b" * (bar_width+1))
    locations = []
    headings = []
    for i in range(number):
        with open(path % i, "r") as f:
            dat = json.load(f)
            locations.append(dat["location"])
            headings.append(dat["heading"])
        if i % (number // bar_width) == bar_width:
            sys.stdout.write("=")
            sys.stdout.flush()
    sys.stdout.write("\n")
    locations = np.array(locations)
    headings = np.array(headings)
    return locations, headings

def plot_location(locations):
    fig = plt.figure()
    ax = fig.gca(projection="3d")
    ax.scatter(
        locations[:,0].transpose(),
        locations[:,1].transpose(),
        locations[:,2].transpose(),
        label="trajectory")
    ax.legend()
    plt.show()

def diffs(locations):
    x_replicated = \
      np.ones(locations[:,0,None].shape).dot(locations[:,0,None].transpose())
    y_replicated = \
      np.ones(locations[:,1,None].shape).dot(locations[:,1,None].transpose())
    z_replicated = \
      np.ones(locations[:,2,None].shape).dot(locations[:,2,None].transpose())
    x_diffs = x_replicated - x_replicated.transpose()
    y_diffs = y_replicated - y_replicated.transpose()
    z_diffs = z_replicated - z_replicated.transpose()

    return x_diffs, y_diffs, z_diffs

def distance(x_d, y_d, z_d):
    return np.sqrt(x_d*x_d + y_d*y_d + z_d*z_d)

def threshold(vals, thres):
    return vals * np.less(vals, thres)

def min_threshold(vals, thres):
    return vals * np.greater(vals, thres)

def angle(x_d, y_d):
    return np.tan(y_d/x_d)

def angle_diff(angles, headings):
    #Convert to radians
    headings = np.expand_dims(np.radians(headings), 1)
    # Replicate column wise
    headings_rep = np.ones(headings.shape).dot(headings.transpose())
    return np.abs(angles - headings_rep)

def disp_pair(image_path, i, j):
    p1 = image_path % i
    p2 = image_path % j
    disp_pair(p1, p2)

def disp_pair(p1, p2):
    print(p1)
    print(p2)
    im1 = cv2.imread(p1)
    im2 = cv2.imread(p2)
    cc = np.concatenate((im1, im2), axis=0)
    cv2.imshow("Pair", cc)

def cluster(locations, start_i):
    m_x = np.max(locations[:, 0]) - np.min(locations[:, 0])
    m_y = np.max(locations[:, 1]) - np.min(locations[:, 1])
    cluster_nums = 6
    cluster_sep_x = m_x // cluster_nums
    cluster_sep_y = m_y // cluster_nums

    # Cluster locations (break down computation)
    clusters_x = {}
    for i, v in enumerate(locations):
        ind = v[0] // cluster_sep_x
        if ind not in clusters_x:
            clusters_x[ind] = []
        clusters_x[ind].append(np.concatenate(([i+start_i], v, [heading[i]])))

    print("Divided along the x axis %i times" % len(clusters_x))
    clusters = {}
    for i in clusters_x:
        clusters_x[i] = np.array(clusters_x[i])
        for v in clusters_x[i]:
            ind = v[2] // cluster_sep_y
            xy_ind = "%i %i" % (i, ind)
            if xy_ind not in clusters:
                clusters[xy_ind] = []
            clusters[xy_ind].append(v)
    print("Created %i clusters" % len(clusters))
    """
    for i in clusters:
        clusters[i] = np.array(clusters[i])
        if clusters[i].shape[0] < MIN_POINTS:
            #clusters.pop(i, None)
            continue
        yield clusters[i]
    """
    return clusters

def single_overlap(row1, row2):
    p1 = create_polygon(row1[1], row1[2], row1[4])
    p2 = create_polygon(row2[1], row2[2], row2[4])
    return calc_overlap(p1, p2)

def poly_overlap(info_arr):
    locations = info_arr[:, [1,2,3]]

    # Threshold distance
    dx, dy, dz = diffs(locations)
    dist = distance(dx, dy, dz)
    tt = threshold(dist, MAX_DIST_THRESHOLD) * min_threshold(dist, MIN_DIST_THRESHOLD)
    nz = np.nonzero(tt)
    xs = nz[0]
    ys = nz[1]

    # Close pairs
    pairs = np.vstack((xs, ys)).transpose()
    filt_pairs = []
    for (x, y) in pairs:
        """
        loc1 = info_arr[x, [1,2,3]]
        head1 = info_arr[x, 4]
        loc2 = info_arr[y, [1,2,3]]
        head2 = info_arr[y, 4]
        x_i, y_i = (info_arr[x, 0], info_arr[y, 0])
        p1 = create_polygon(loc1[0], loc1[1], head1)
        p2 = create_polygon(loc2[0], loc2[1], head2)
        overlap = calc_overlap(p1, p2)
        """
        overlap = poly_overlap(info_arr[x], info_arr[y])
        if overlap > OVERLAP_THRESHOLD:
            filt_pairs.append([x_i, y_i])
            #disp_pair(paths[1][0], x_i, y_i)
    return filt_pairs

def angle_overlap(info_arr):
    locations = info_arr[:, [1,2,3]]
    heading = info_arr[:, 4]
    dx, dy, dz = diffs(locations)
    dist = distance(dx, dy, dz)
    ang = angle(dx, dy)
    da = angle_diff(ang, heading)
    reverse_eye = np.abs(1 - np.eye(np.shape(da)[0]))
    tt = threshold(dist, DIST_THRESHOLD) * threshold(da, ANGLE_THRESHOLD) * reverse_eye
    tt[np.isnan(tt)] = 0
    nz = np.nonzero(tt)
    xs = nz[0]
    ys = nz[1]
    pairs = np.vstack((xs, ys)).transpose()
    pairsc = []
    for i in pairs:
        x, y = i
        x_i, y_i = (info_arr[x, 0], info_arr[y, 0])
        pairsc.append([x_i, y_i])
        #disp_pair(paths[1][0], x_i, y_i)
    return pairsc

start_i = 0
clusters = {}
for (meta_path, img_path, count) in paths:
    # [x, y, z], r
    locations, heading = load_metadata(meta_path, count)
    #plot_location(locations)
    cs = cluster(locations, start_i)
    for i in cs:
        if i in clusters:
            clusters[i] = np.vstack((clusters[i], cs[i]))
        else:
            clusters[i] = np.array(cs[i])
    start_i += count

pairs = []
for i in clusters:
    print(i)
    locs = clusters[i][:, [1,2,3]]
    #plot_location(locs)
    #pairs = angle_overlap(i)
    pairs_i = poly_overlap(clusters[i])
    for [x, y] in pairs_i:
        # Calc the run and the within run index
        x_i = 0
        x_wi = x
        while True:
            if paths[x_i][2] < x_wi:
                x_wi -= paths[x_i][2]
                x_i += 1
            else:
                break

        # Calc the run and the within run index
        y_i = 0
        y_wi = y
        while True:
            if paths[y_i][2] < y_wi:
                y_wi -= paths[y_i][2]
                y_i += 1
            else:
                break
        pairs.append([paths[x_i][1] % x_wi, paths[y_i][1] % y_wi])

pairs = np.array(pairs)
print("Found %i pairs" % pairs.shape[1])
print(pairs)
np.save("positive_pairs.npy", pairs)
for [p1, p2] in pairs:
    disp_pair(p1, p2)
    if cv2.waitKey(0) & 0xFF == ord("q"):
        break
